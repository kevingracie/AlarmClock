package com.kg.alarmclock.event

import com.kg.alarmclock.model.AlarmClockEntity

data class AlarmClockEvent(var position: Int, var entity: AlarmClockEntity)