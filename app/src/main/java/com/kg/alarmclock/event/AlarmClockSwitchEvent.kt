package com.kg.alarmclock.event

data class AlarmClockSwitchEvent(
    var isCheck: Boolean,
    var position: Int
)