package com.kg.alarmclock.event

import com.kg.alarmclock.model.AlarmClockEntity

data class AlarmClockDeleteEvent(var entity: AlarmClockEntity)