package com.kg.alarmclock.service

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast
import com.kg.alarmclock.utils.StringUtils

class AlarmClockReceiver : BroadcastReceiver() {

    companion object {
        val TIMEMILLIS = "timeMillis"
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        var timeMillis = intent!!.getLongExtra(TIMEMILLIS, 0L)
        var time = StringUtils.getHHmm(timeMillis)

        Toast.makeText(context, "您设置的时间${time}到了", Toast.LENGTH_SHORT).show()
    }
}