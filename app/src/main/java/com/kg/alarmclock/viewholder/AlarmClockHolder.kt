package com.kg.alarmclock.viewholder

import android.annotation.SuppressLint
import android.view.View
import android.widget.Switch
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.kg.alarmclock.Constant
import com.kg.alarmclock.R
import com.kg.alarmclock.event.AlarmClockEvent
import com.kg.alarmclock.event.AlarmClockSwitchEvent
import com.kg.alarmclock.model.AlarmClockEntity
import com.kg.alarmclock.utils.RxBus
import com.steve.creact.annotation.DataBean
import com.steve.creact.library.viewholder.BaseRecyclerViewHolder
import java.util.*

/**
 * 疾病列表
 */
@DataBean(beanName = "AlarmClockBean", data = AlarmClockEntity::class)
class AlarmClockHolder(itemView: View) : BaseRecyclerViewHolder<AlarmClockEntity>(itemView) {

    companion object {
        const val LAYOUT_ID = R.layout.item_list_alarm_clock
    }

    @SuppressLint("SetTextI18n")
    override fun setData(data: AlarmClockEntity?) {
        if (data == null)
            return

        val switch_clock = getView<Switch>(R.id.switch_clock)
        val time = getView<TextView>(R.id.time)
        val date_list = getView<TextView>(R.id.date_list)

        var isChoose = data.isChoose!!
        var hour = data.hour.toString()
        var minute = data.minute.toString()
        var dates = data.dates as ArrayList<Int>

        switch_clock.isChecked = isChoose
        if (isChoose)
            time.setTextColor(ContextCompat.getColor(context, R.color.black))
        else
            time.setTextColor(ContextCompat.getColor(context, R.color.text_name_default))

        switch_clock.setOnCheckedChangeListener { compoundButton, isChecked ->
            if (isChecked)
                time.setTextColor(ContextCompat.getColor(context, R.color.black))
            else
                time.setTextColor(ContextCompat.getColor(context, R.color.text_name_default))

            RxBus.INSTACE.send(AlarmClockSwitchEvent(isChecked, adapterPosition))
        }

        if (hour.length == 1)
            hour = "0$hour"

        if (minute.length == 1)
            minute = "0$minute"

        time.text = "$hour:$minute"

        var date_type = dates[dates.size - 1]
        var date_text: CharSequence

        if (date_type == Constant.NEXT) {
            val calendar = Calendar.getInstance()
            var currentHour = calendar.get(Calendar.HOUR_OF_DAY)
            var currentMinute = calendar.get(Calendar.MINUTE)

            if (currentHour < data.hour || (currentHour == data.hour && currentMinute < data.minute)) {
                date_text = Constant.DATE_MAP[Constant.TODAY].toString()
            } else
                date_text = Constant.DATE_MAP[Constant.NEXT].toString()
        } else if (date_type == Constant.EVERY) {
            date_text = Constant.DATE_MAP[Constant.EVERY].toString()
        } else {
            var sb = StringBuilder()
            for (i in 0 until dates.size - 1) {
                var date = dates[i]
                if (date > 0) {
                    sb.append(Constant.DATE_MAP[date].toString() + "  ")
                }
            }
            date_text = sb.toString()
        }

        date_list.text = date_text

        setOnClickListener(R.id.layout) {
            RxBus.INSTACE.send(AlarmClockEvent(adapterPosition, data))
        }
    }
}
