package com.kg.alarmclock.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseActivity : AppCompatActivity() {

    private var compositeDisposable = CompositeDisposable()

    protected abstract fun getLayout(): Int

    protected abstract fun initView()

    fun addSubscribe(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    fun unAllSubscribe() {
        compositeDisposable.clear()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(getLayout())
        initView()
    }

    override fun onDestroy() {
        super.onDestroy()
        unAllSubscribe()
    }
}