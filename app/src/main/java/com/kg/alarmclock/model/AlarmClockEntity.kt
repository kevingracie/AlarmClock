package com.kg.alarmclock.model

import java.io.Serializable

/**
 *  AlarmClock list 的 model
 */
class AlarmClockEntity : Serializable {
    var minute: Int = 0
    var hour: Int = 0
    var isChoose: Boolean = false
    var dates: List<Int>? = null
    var dateValues: List<Int>? = null
    var pendingIntentRequestCode = 0
    var action: String? = null
}