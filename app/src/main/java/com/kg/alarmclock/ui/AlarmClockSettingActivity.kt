package com.kg.alarmclock.ui

import android.content.Intent
import com.kg.alarmclock.R
import com.kg.alarmclock.base.BaseActivity
import com.kg.alarmclock.event.AlarmClockDeleteEvent
import com.kg.alarmclock.model.AlarmClockEntity
import com.kg.alarmclock.utils.RxBus
import kotlinx.android.synthetic.main.activity_alarm_clock_setting.*
import java.util.*
import kotlin.collections.ArrayList

/**
 * 设置闹钟
 */
class AlarmClockSettingActivity : BaseActivity() {

    companion object {
        val ENTITY = "entity"
        val ENTITY_SAVE = "entity_save"

        val TYPE = "type"
        val TYPE_ADD = 0
        val TYPE_EDIT = 1
    }

    private var type = TYPE_ADD
    private var entity: AlarmClockEntity? = null
    private var dates = arrayOf(0, 0, 0, 0, 0, 0, 0, 0)
    private var dateValues = ArrayList<Int>()
    private var totalData = 0
    private var pendingIntentRequestCode = 0
    private var action = ""

    private var hour = 0
    private var minute = 0

    override fun getLayout(): Int {
        return R.layout.activity_alarm_clock_setting
    }

    override fun initView() {
        type = intent.getIntExtra(TYPE, TYPE_ADD)

        back.setOnClickListener {
            finish()
        }

        save.setOnClickListener {
            dates[dates.size - 1] = totalData

            var entity = AlarmClockEntity()
            entity.hour = timepicker.currentHour
            entity.minute = timepicker.currentMinute
            entity.isChoose = true
            entity.dates = dates.toList()
            entity.pendingIntentRequestCode = pendingIntentRequestCode
            entity.action = action
            entity.dateValues = dateValues

            setResult(RESULT_OK, Intent().putExtra(ENTITY_SAVE, entity))
            finish()
        }

        delete.setOnClickListener {
            when (type) {
                TYPE_EDIT -> {
                    RxBus.INSTACE.send(AlarmClockDeleteEvent(entity!!))
                }
            }

            finish()
        }

        timepicker.setIs24HourView(true)

        initTime()
        initCheckBox()

        when (type) {
            TYPE_ADD -> {
                updateAddTime()
            }

            TYPE_EDIT -> {
                entity = intent.getSerializableExtra(ENTITY) as AlarmClockEntity
                updateEditTime()
                updateEditDates()
            }
        }

        timepicker.currentHour = hour
        timepicker.currentMinute = minute
    }

    private fun initTime() {
        timepicker.setOnTimeChangedListener { _, hourOfDay, minute ->
            timepicker.currentHour = hourOfDay
            timepicker.currentMinute = minute
        }
    }

    private fun updateAddTime() {
        var calendar = Calendar.getInstance()
        hour = calendar!!.get(Calendar.HOUR_OF_DAY)
        minute = calendar!!.get(Calendar.MINUTE)
    }

    private fun updateEditTime() {
        if (entity == null)
            return

        hour = entity!!.hour
        minute = entity!!.minute
        pendingIntentRequestCode = entity!!.pendingIntentRequestCode
        action = entity!!.action!!
    }

    private fun updateEditDates() {
        if (entity == null)
            return

        var dates = entity!!.dates as ArrayList<Int>
        dates.removeAt(dates.size - 1)

        for (date in dates) {
            when (date) {
                1 -> one.isChecked = true
                2 -> two.isChecked = true
                3 -> three.isChecked = true
                4 -> four.isChecked = true
                5 -> five.isChecked = true
                6 -> six.isChecked = true
                7 -> seven.isChecked = true
            }
        }
    }

    private fun initCheckBox() {
        one.setOnCheckedChangeListener { _, isCheck ->
            updateCheck(0, isCheck)
        }

        two.setOnCheckedChangeListener { _, isCheck ->
            updateCheck(1, isCheck)
        }

        three.setOnCheckedChangeListener { _, isCheck ->
            updateCheck(2, isCheck)
        }

        four.setOnCheckedChangeListener { _, isCheck ->
            updateCheck(3, isCheck)
        }

        five.setOnCheckedChangeListener { _, isCheck ->
            updateCheck(4, isCheck)
        }

        six.setOnCheckedChangeListener { _, isCheck ->
            updateCheck(5, isCheck)
        }

        seven.setOnCheckedChangeListener { _, isCheck ->
            updateCheck(6, isCheck)
        }
    }

    private fun updateCheck(position: Int, isCheck: Boolean) {
        var positionValue = position + 1

        if (isCheck) {
            dates[position] = positionValue
            dateValues.add(positionValue)
            totalData += positionValue
        } else {
            dates[position] = 0
            dateValues.remove(positionValue)
            totalData -= positionValue
        }
    }
}
