package com.kg.alarmclock

object Constant {
    val ONE = 1
    val TWO = 2
    val THREE = 3
    val FOUR = 4
    val FIVE = 5
    val SIX = 6
    val SEVEN = 7
    val EVERY = 28
    val NEXT = 0
    val TODAY = 8

    val ONE_DAY = 24 * 60 * 60 * 1000L
    val ONE_WEEK = 24 * 60 * 60 * 1000 * 7L

    val DATE_MAP = mapOf(
        ONE to "周一",
        TWO to "周二",
        THREE to "周三",
        FOUR to "周四",
        FIVE to "周五",
        SIX to "周六",
        SEVEN to "周日",
        NEXT to "明天",
        TODAY to "今天",
        EVERY to "每天"
    )

    val CACHE_PATH = "ALARM_CLOCK.dat"
}