package com.kg.alarmclock.utils

import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers

enum class RxBus {

    INSTACE;

    private val _bus = PublishRelay.create<Any>().toSerialized()

    fun send(o: Any) {
        if (hasObservers()) {
            try {
                _bus.accept(o)
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    fun asFlowable(): Flowable<Any> {
        return _bus.toFlowable(BackpressureStrategy.LATEST)
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun hasObservers(): Boolean {
        return _bus.hasObservers()
    }
}