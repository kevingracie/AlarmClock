package com.kg.alarmclock.utils

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class StringUtils {

    companion object {
        val calendar = Calendar.getInstance()

        /**
         * 获取时间戳
         */
        @Throws(ParseException::class)
        fun dateToStamp(time: String): Long {
            var year = calendar.get(Calendar.YEAR)
            var month = calendar.get(Calendar.MONTH) + 1
            var day = calendar.get(Calendar.DAY_OF_MONTH)

            var timeParse = "$year-$month-$day-$time"

            val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd-HH-mm")
            val date = simpleDateFormat.parse(timeParse)
            val ts = date.time
            return ts
        }

        fun getHHmm(timeMillis: Long): String {
            val simpleDateFormat = SimpleDateFormat("HH:mm")
            val date = Date(timeMillis)
            return simpleDateFormat.format(date)
        }

        /**
         * 获取星期几
         */
        fun getWeek(): Int {
            var dayValue = 0
            var day = calendar.get(Calendar.DAY_OF_WEEK)
            when (day) {
                1 -> dayValue = 7
                2 -> dayValue = 1
                3 -> dayValue = 2
                4 -> dayValue = 3
                5 -> dayValue = 4
                6 -> dayValue = 5
                7 -> dayValue = 6
            }
            return dayValue
        }
    }
}